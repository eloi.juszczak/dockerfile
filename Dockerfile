FROM mysql:5
COPY atomikos_one.sql /home
COPY atomikos_two.sql /home
USER root
RUN "mysql atomikos_one < atomikos_one.sql"
RUN "mysql atomikos_two < atomikos_two.sql"